import { alchemy } from "@config/AlchemyConfig";
import { ClawMachine, ClawMachine__factory } from "@typechain/ethers-contracts";
import { Nft, OwnedNftsResponse } from "alchemy-sdk";
import { ethers } from "ethers";
import { useEffect, useState } from "react";
import { useAccount, useSigner } from "wagmi";

function useNfts(owner: string) {
    const [nfts, setNfts] = useState<Nft[]>();
    const [prizes, setPrizes] = useState<ClawMachine.PrizeStructOutput[]>();

    const { isConnected } = useAccount();
    const { data: signer } = useSigner();

    let clawMachine: ClawMachine;

    useEffect(() => {
        if (isConnected != null) {
            alchemy.nft
            .getNftsForOwner(owner)
            .then((response: OwnedNftsResponse) => {
              console.log(response);
              setNfts(response.ownedNfts);
            })
            .catch((err) => console.error(err));
        }
    }, [isConnected]);

    useEffect(() => {
        if(owner === process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS && nfts && nfts.length > 0)
            getPrizes();
    }, [nfts]);
    
    async function getPrizes() {
        if(signer == null)
        {
            console.error("Signer not found !");
            return;
        }
        let prizeIds: string[] = [];
        for(let nft of nfts!)
            prizeIds.push(ethers.utils.keccak256(ethers.utils.defaultAbiCoder.encode([ "address", "uint" ], [ nft.contract.address, nft.tokenId ])));
        
        clawMachine = ClawMachine__factory.connect(process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!, signer);
    
        let result = await clawMachine.getPrizes(prizeIds);
    
        setPrizes(result);
    }

    return {nfts, prizes};
}

export default useNfts;