export type navLinkType = {
  id: string;
  name: string;
  url: string;
  element: string;
};

const navLinksData: Array<navLinkType> = [
  {
    id: "0",
    name: "Profile",
    url: "/profile",
    element: "profile",
  },
  {
    id: "1",
    name: "My nfts",
    url: "/List",
    element: "list_nft",
  },
  {
    id: "2",
    name: "Ono ball",
    url: "/AllPrizes",
    element: "all_prizes",
  },
  {
    id: "3",
    name: "Play",
    url: "/Games",
    element: "games",
  },
  {
    id: "4",
    name: "twitter",
    url: "https://twitter.com/ball_ono",
    element: "twitter",
  },
  {
    id: "5",
    name: "discord",
    url: "https://discord.com/invite/EKG7m3RXnk",
    element: "discord",
  },
];

export default navLinksData;
