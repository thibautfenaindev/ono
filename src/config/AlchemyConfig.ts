import { Alchemy, Network } from "alchemy-sdk";

export const config = {
  apiKey: process.env.NEXT_PUBLIC_ALCHEMY_KEY,
  network: Network.MATIC_MUMBAI,
};

export const alchemy = new Alchemy(config);
