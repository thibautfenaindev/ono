const metaTag = {
  language: "en",
  title: "ONO Beta",
  description:
    "Next Template in typescript used for creating a new project only",
  keywords:
    "NextJs, front-end engineer, web developer, javascript, Taiwan, singapore",
  url: process.env.NEXT_PUBLIC_SITE_URL,
  name: "Ono Team",
  location: "Globe",
  email: "test@gmail.com",
  github_account: "https://github.com/test",
  twitter_account: "@test",
  type: "website",
};

export default metaTag;
