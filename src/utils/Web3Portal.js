import { useEffect } from "react";
import { useWeb3React } from "@web3-react/core";
import { InjectedConnector } from "@web3-react/injected-connector";
import Web3 from 'web3';


import ClawMachine from "@config/clawMachine.json"
import ERC721 from "@config/abi/ERC721.json";
import { toHex, truncateAddress } from "./utils";
import { result } from "lodash";
import { wallet } from "@rainbow-me/rainbowkit";
export const CLAW_MACHINE_ADDRESS = "0x72cfDe3234535636eCab2728594ECA5932eefC30"
export const Injected = new InjectedConnector({
  supportedChainIds: [1, 3, 4, 5, 42, 137, 80001],
});

export let walletActivate = (Injected) => {
  console.log("test wallet", walletActivate);
};
export let walletDeactivate = () => {};
export let walletActive = false;
export let walletChainId = null;
export let walletAccount = null;
export let walletLibrary = null;
export let walletError = null;

function useWeb3Portal() {
  const { activate, deactivate, active, chainId, account, library, error } =
    useWeb3React();

  useEffect(() => {
    Injected.isAuthorized()
      .then((isAuthorized) => {
        if (isAuthorized && !active && !error) {
          activate(Injected);
        }
      })
      .catch(() => {});
  }, [activate, active, error, chainId]);

  useEffect(() => {
    if (active && chainId != 80001) {
      switch_network();
    }
  }, [chainId]);

  walletActivate = activate;
  walletDeactivate = deactivate;
  walletActive = active;
  walletChainId = chainId;
  walletAccount = account;
  walletLibrary = library;
  walletError = error;
};

export const switch_network = () => {
  walletLibrary.provider.request({
    method: "wallet_switchEthereumChain",
    params: [{ chainId: toHex(80001) }],
  });
};

export const check_balance = async (setWalletBalance ) => {
  walletLibrary.provider
    .request({
      method: "eth_getBalance",
      params: [walletAccount, "latest"],
    })
    .then((result) => {
      setWalletBalance((result / 10 ** 18).toPrecision(4));
      return result;
    });
};

let isInitialized = false;
let clawMachine;
let erc721;


export const init = async () => {
    let provider = window.ethereum;
    if (typeof provider !== 'undefined') {
    
    provider
    .request({method: 'eth_requestAccounts' })
    .then((accounts) => {
      selectedAccount = accounts[0];
      console.log(`Selected account is ${selectedAccount}`);
    })
    .catch((err) => {
      console.log(err);
      return;
    });
    window.ethereum.on('accountsChanged', function (accounts){
      selectedAccount = accounts[0];
      console.log(`Selected account changed to ${selectedAccount}`);
    });
  }
  const web3 = new Web3(provider);
  const networkId = await web3.eth.net.getId();
  clawMachine = new web3.eth.Contract(ClawMachine, CLAW_MACHINE_ADDRESS)
  isInitialized = true;
};



/**
 * Play game function
 * @param {*} walletAccount 
 */
export const playGame = async (walletAccount) => {
  console.log("test", walletAccount);

  if (!isInitialized) {
    await init();
  }
  
  const allPrices = await listPrices();

  const playGame = clawMachine.methods.playGame(10, [0,1]).send({from: walletAccount}).on('transactionHash', function(hash){
    console.log('transactionHash', hash);
    alert('you won one ONO NFT, please go to the NFT to see your gift!, it can takes a few minutes before the transaction is completed')
})
.on('receipt', function(receipt){
    console.log('receipt');
})
.on('confirmation', function(confirmationNumber, receipt) {
    console.log('confirmation', confirmationNumber , receipt);
    showFilesWithMessage();
})
.on('error', console.error); // If a out of gas error, the second parameter is the receipt.

}


/**
 * List nft inside the Claw machine
 */
export const listPrices = async() =>{
  if (!isInitialized) {
    await init();
  }
  const allPrices = await clawMachine.methods.getAllPrizes().call();
  console.log('list', allPrices)

  return allPrices;
}

/**
 * Deposit an NFT into the Claw Machine
 * @param {*} walletAccount 
 */
export const depositNft = async (walletAccount, nftAddress, expectedIncome, tokenId) => {
  console.log("test", walletAccount, nftAddress, expectedIncome, tokenId);

  if (!isInitialized) {
    await init();
  }
    // get nft adress

    // connect to the smart contract
    const web3 = new Web3(window.ethereum);
    const networkId = await web3.eth.net.getId();
    erc721 = new web3.eth.Contract(ERC721,nftAddress )

     //safeTransferFrom => adress nft/owner to ClawMachine
     const owner = await erc721.methods.ownerOf(tokenId).call();
    const r = await erc721.methods.safeTransferFrom(walletAccount, CLAW_MACHINE_ADDRESS,tokenId).encodeABI()
    console.log('test owner', owner)

    console.log('test transfer', r)

   
} 



export {useWeb3Portal};