import { ClawMachine } from "@typechain/ethers-contracts";
import { Nft } from "alchemy-sdk";
import { ethers } from "ethers";
import {
  Card,
  ImageNft,
  Name,
  NameContainer,
  ImageContainer,
  RewardTitle,
  RewardContainer,
  RewardScore,
  RewardUnit,
} from "./SingleNft.styles";

type SingleNftProps = {
  nft: Nft;
  prize?: ClawMachine.PrizeStructOutput | undefined;
  isClicked: boolean;
  onClick: () => void;
};

function SingleNft(props: SingleNftProps) {
  const isIpfs = (imgSrc: string | undefined) => {
    if (imgSrc?.startsWith("ipfs://")) {
      console.log("https://ipfs.io/ipfs/" + imgSrc.slice(6));

      return "https://ipfs.io/ipfs" + imgSrc.slice(6);
    } else return imgSrc;
  };

  return (
    <Card isClicked={props.isClicked} onClick={props.onClick}>
      <ImageContainer>
        <ImageNft
          src={isIpfs(props.nft.rawMetadata?.image)}
          alt={props.nft.title}
          isClicked={props.isClicked}
          width={247}
          height={247}
        />
      </ImageContainer>
      <NameContainer>
        <Name>{props.nft.title}</Name>
      </NameContainer>
      {props.prize && (
        <>
          <RewardTitle>Expected Income : </RewardTitle>
          <RewardContainer>
            <RewardScore>
              {ethers.utils.formatEther(props.prize.expectedIncome)}
            </RewardScore>
            <RewardUnit>ONO</RewardUnit>
          </RewardContainer>
        </>
      )}
    </Card>
  );
}

export default SingleNft;
