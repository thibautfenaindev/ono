import Image from "next/image";
import styled from "styled-components";

export const Card = styled.div<{ isClicked: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 250px;
  padding: 0 5;
  border-color: ${(props) => (props.isClicked ? "#ff6f5c" : "#ffffff")};
  border-width: 2px;
  border-style: solid;
  position: relative;
  border-radius: 15px;
  margin: 5px;
  margin-right: 15px;
  margin-bottom: 20px;
`;

// export const NftImage = styled(Image)
export const ImageContainer = styled.div`
  position: relative;
  top: 0;
  left: 0;
`;
export const ImageNft = styled.img<{ isClicked: boolean }>`
  border-radius: 15px;
  border: 2px;
  border-color: ${(props) => (props.isClicked ? "#ff6f5c" : "#ffffff")};
  border-style: solid;
  top: 0;
  left: 0;
`;

export const HeartContainer = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
`;
export const Heart = styled(Image)``;

export const ButtonContainer = styled.div`
  position: absolute;
  top: 500px;
`;

export const NameContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 40px;
`;

export const Name = styled.div`
  margin-top: 10px;
  font-family: "Arial";
  position: absolute;
  font-style: normal;
  color: #ffffff;
  font-weight: 700;
  font-size: 12.5px;
  left: 10px;
`;

export const ListedTime = styled.div`
  font-family: "Arial";
  margin-top: 10px;
  font-style: normal;
  position: absolute;
  right: 10px;
  color: #c9c9c9;
  font-weight: 400;
  font-size: 10px;
  justify-content: end;
  align-items: flex-end;
`;

export const RewardContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  margin-bottom: 20px;
`;

export const RewardTitle = styled.div`
  font-style: normal;
  font-size: 1em;
  color: #ff6f5c;
`;

export const RewardScore = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 1em;
  color: #ff6f5c;
  vertical-align: bottom;
`;

export const RewardUnit = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 10px;
  vertical-align: bottom;
  margin-bottom: 5px;
  margin-left: 5px;
  color: #ff6f5c;
`;

export const CollectionContainer = styled.div`
  display: flex;
  align-self: flex-start;
  margin-left: 10px;
  flex-direction: row;
  margin-bottom: 10px;
`;

export const ProgressContainer = styled.div`
  display: flex;
  margin-left: 10px;
  width: 100%;
  flex-direction: column;
  margin-bottom: 30px;
`;

export const CollectionTitle = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 9.9426px;
  line-height: 10px;
  color: #ffffff;
`;

export const CollectionScore = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 28px;
  line-height: 29px;
  color: #ffffff;
`;

export const OpenseaButton = styled.button`
  border-radius: 6px;
  padding: 5px;
  color: white;
  border: 1px;
  background-color: #ff6f5c;
  margin-top: 10px;
  margin-bottom: 5px;
`;

export const Description = styled.div`
  font-style: normal;
  margin-top: 10px;
  font-weight: 400;
  max-width: 320px;
  font-size: 12px;
`;
