import EnergyBar from "@components/EnergyBar/EnergyBar";
import { Alert } from "@components/Grommets/Alert/Alert";
import { ProgressModal } from "@components/Grommets/ProgressModal/ProgressModal";
import { ClawMachine, ClawMachine__factory } from "@typechain/ethers-contracts";
import { Nft } from "alchemy-sdk";
import { BigNumber, BytesLike, ethers } from "ethers";
import { useRouter } from "next/router";
import { AppContext } from "pages/_app";
import {
  useCallback,
  useContext,
  useEffect,
  useLayoutEffect,
  useState,
} from "react";
import { Unity, useUnityContext } from "react-unity-webgl";
import { useSigner } from "wagmi";
import { BarContainer, Container } from "../Games.styles";
import PrizeList from "./PrizeList";

type ClawMachineGameProps = {
  nfts: Nft[] | undefined;
  prizes: ClawMachine.PrizeStructOutput[] | undefined;
};

enum EState {
  selectPrizes,
  playGame,
  isPlaying,
  lose,
  win,
  noStaked,
}

function ClawMachineGame(props: ClawMachineGameProps) {
  const { data: signer, isError, error } = useSigner();
  const [counter, setCounter] = useState(4);
  const [selectedPrizes, setSelectedPrizes] = useState<boolean[]>();
  const [bet, setBet] = useState<BigNumber>();
  const [state, setState] = useState(EState.selectPrizes);
  const [prizeWon, setPrizeWon] = useState(0);
  const [balance, setBalance] = useState<number>(0);
  const router = useRouter();
  const appContext = useContext(AppContext);

  const root = "unity/clawMachineGame/Build";

  const {
    unityProvider,
    addEventListener,
    removeEventListener,
    UNSAFE__detachAndUnloadImmediate: detachAndUnloadImmediate,
  } = useUnityContext({
    loaderUrl: root + ".loader.js",
    dataUrl: root + ".data",
    frameworkUrl: root + ".framework.js",
    codeUrl: root + ".wasm",
  });

  useEffect(() => {
    if (selectedPrizes && selectedPrizes.length > 0) setState(EState.playGame);
    else setState(EState.selectPrizes);
  }, [selectedPrizes]);

  useEffect(() => {
    return () => {
      detachAndUnloadImmediate();
    };
  }, [detachAndUnloadImmediate]);

  useLayoutEffect(() => {
    if (sessionStorage.getItem("energy")) {
      setCounter(parseInt(sessionStorage.getItem("energy")!));
      console.log("Energy : " + sessionStorage.getItem("energy"));
    } else {
      sessionStorage.setItem("energy", counter.toString());
      console.log("Counter empty");
    }
  }, []);

  const handleEndGame = useCallback(() => {
    setState(EState.isPlaying);
  }, []);

  useEffect(() => {
    if (state === EState.isPlaying) playGame();
  }, [state]);

  useEffect(() => {
    if (signer) getBalance();
  }, [signer]);

  useEffect(() => {
    addEventListener("EndGame", handleEndGame);
    return () => {
      removeEventListener("EndGame", handleEndGame);
    };
  }, [addEventListener, removeEventListener, handleEndGame]);

  async function getBalance() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    const clawMachine = ClawMachine__factory.connect(
      process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
      signer
    );
    try {
      let b = await clawMachine.onoDeposit(signer.getAddress());
      setBalance(parseFloat(ethers.utils.formatEther(b)));
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    }
  }

  async function playGame() {
    if (isError) {
      console.error(error);
      return;
    }
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    setState(EState.isPlaying);
    const clawMachine = ClawMachine__factory.connect(
      process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
      signer
    );

    clawMachine.on("NoPrizeStaked", () => {
      setState(EState.noStaked);
    });
    clawMachine.on("Win", (prizeId: string) => {
      if (!props.prizes) return;
      for (let i = 0; i < props.prizes?.length; i++) {
        if (prizeId === getPrizeId(props.prizes[i])) {
          setPrizeWon(i);
          break;
        }
      }
      setState(EState.win);
    });
    clawMachine.on("Lose", () => {
      setState(EState.lose);
    });
    try {
      console.log("bet : " + bet);
      let transaction = await clawMachine.playGame(bet!, getPrizeList());
      let receipt = await transaction.wait();
      console.log(receipt);

      let c = parseInt(sessionStorage.getItem("energy")!);
      c--;
      sessionStorage.setItem("energy", c.toString());
      setCounter(c);
      console.log("End game");
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
      setState(EState.playGame);
    }
  }

  function getPrizeList(): BytesLike[] {
    if (!selectedPrizes || !props.prizes) return [];

    let result: BytesLike[] = [];
    for (let i = 0; i < selectedPrizes.length; i++) {
      if (!selectedPrizes[i]) continue;
      result.push(getPrizeId(props.prizes[i]));
    }
    console.log(result);
    return result;
  }

  const isIpfs = (imgSrc: string | undefined) => {
    if (imgSrc?.startsWith("ipfs://"))
      return "https://ipfs.io/ipfs" + imgSrc.slice(6);
    return imgSrc;
  };

  const getPrizeId = (prize: ClawMachine.PrizeStructOutput) =>
    ethers.utils.keccak256(
      ethers.utils.defaultAbiCoder.encode(
        ["address", "uint"],
        [prize.nftAddress, prize.tokenId]
      )
    );

  let body;

  switch (state) {
    case EState.selectPrizes:
      body = (
        <PrizeList
          nfts={props.nfts}
          prizes={props.prizes}
          setBet={(value) => setBet(value)}
          confirm={(value) => setSelectedPrizes(value)}
          balance={balance}
        />
      );
      break;
    case EState.isPlaying:
      body = <ProgressModal text="In progress..." />;
      break;
    case EState.noStaked:
      body = (
        <Alert
          title="Error"
          text="No NFT selected are available ?"
          onClose={() => router.push("/")}
        ></Alert>
      );
      break;
    case EState.lose:
      body = <Alert title="You lose" onClose={() => router.push("/")}></Alert>;
      break;
    case EState.win:
      body = (
        <Alert
          title="You win"
          text={props.nfts ? props.nfts[prizeWon].title : ""}
          onClose={() => router.push("/")}
        >
          <img
            src={
              props.nfts ? isIpfs(props.nfts[prizeWon].rawMetadata?.image) : ""
            }
            width={100}
            height={100}
            style={{ borderRadius: "15px", marginBottom: "10px" }}
          />
        </Alert>
      );
      break;
  }

  return (
    <Container>
      <BarContainer>
        <EnergyBar maxValue={4} value={counter} />
      </BarContainer>
      <Unity
        unityProvider={unityProvider}
        style={{
          width: "50vw",
          height: "60vh",
          borderRadius: "6px",
          display: state === EState.playGame ? "block" : "none",
        }}
      />
      {counter > 0 ? (
        body
      ) : (
        <p>
          You can't play this game anymore.
          <br />
          Refill your electro to play again
        </p>
      )}
    </Container>
  );
}

export default ClawMachineGame;
