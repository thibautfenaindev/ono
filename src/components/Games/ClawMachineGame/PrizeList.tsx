import SingleNft from "@components/SingleNft/SingleNft";
import { Nft } from "alchemy-sdk";
import { useEffect, useState } from "react";
import {
  Container,
  NftContainer,
  Title,
} from "@components/ListNft/ListNft.styles";
import { ClawMachine } from "@typechain/ethers-contracts";
import { useAccount } from "wagmi";
import { TextInput, Button } from "grommet";
import { BigNumber, ethers } from "ethers";

type ListNFTProps = {
  nfts: Nft[] | undefined;
  prizes: ClawMachine.PrizeStructOutput[] | undefined;
  balance: number;
  setBet: (bet: BigNumber) => void;
  confirm: (prizes: boolean[]) => void;
};

function ListNFT(props: ListNFTProps) {
  const [selectedPrizes, setSelectedPrizes] = useState<boolean[]>();
  const { address } = useAccount();
  const [valid, setValid] = useState(false);

  useEffect(() => {
    if (props.nfts && props.nfts.length > 0)
      setSelectedPrizes(Array.from({ length: props.nfts.length }, () => false));
  }, [props.nfts]);

  if (props.nfts && props.prizes) {
    return (
      <Container>
        <Title>Choose your NFTs</Title>
        Your bet :
        <TextInput
          plain
          type="number"
          onChange={(event) => {
            try {
              let value = ethers.utils.parseEther(event.target.value);
              props.setBet(value);
              setValid(value.gt(0));
            } catch (error) {}
          }}
          style={{ border: "1px solid red" }}
          min="0"
          max={props.balance}
        />
        <NftContainer>
          {Object.values(props.nfts).map(
            (nft, i) =>
              props.prizes &&
              selectedPrizes &&
              props.prizes[i].ownerAddress !== address &&
              props.prizes[i].status !== 0 && (
                <SingleNft
                  nft={nft}
                  key={i}
                  isClicked={selectedPrizes[i]}
                  prize={props.prizes ? props.prizes[i] : undefined}
                  onClick={() => {
                    setSelectedPrizes((sp) => {
                      if (sp)
                        return [
                          ...sp.slice(0, i),
                          sp[i] ? false : true,
                          ...sp.slice(i + 1),
                        ];
                      return [];
                    });
                    console.log(selectedPrizes);
                  }}
                />
              )
          )}
        </NftContainer>
        {selectedPrizes && (
          <Button
            primary
            label="Play"
            disabled={!valid || !selectedPrizes.some((e) => e)}
            color="red"
            onClick={() => props.confirm(selectedPrizes)}
            style={{ margin: "30px" }}
          />
        )}
      </Container>
    );
  } else {
    return <div> Nothing to show ...</div>;
  }
}
Array.apply;

export default ListNFT;
