import NftModal from "@components/NftModal/NftModal";
import SingleNft from "@components/SingleNft/SingleNft";
import { ClawMachine } from "@typechain/ethers-contracts";
import { Nft } from "alchemy-sdk";
import { useState } from "react";
import { useAccount } from "wagmi";
import { Container, NftContainer, Title } from "./ListNft.styles";

type ListNFTProps = {
  id: string,
  title: string,
  nfts: Nft[] | undefined,
  prizes: ClawMachine.PrizeStructOutput[] | undefined,
  visibility: boolean,
};

function ListNFT(props: ListNFTProps) {
  const [nftModal, setNftModal] = useState<Nft>();
  const [prizeModal, setPrizeModal] = useState<ClawMachine.PrizeStructOutput>();
  const [showModal, setShowModal] = useState(false);

  const { address } = useAccount();

  if (props.nfts != null) {
    return (
      <Container>
        <Title>{props.title}</Title>
        {showModal && (
          <NftModal
            isOpen={showModal}
            nft={nftModal}
            toggleModal={() => setShowModal(false)}
            prize={prizeModal}
          />
        )}
        <NftContainer>
          {Object.values(props.nfts).map((nft, i) => (
            ((props.prizes && 
              (props.prizes[i].ownerAddress === address || props.prizes[i].status === 1)) 
              || !props.prizes) &&
            <SingleNft
              nft={nft}
              key={i}
              isClicked={false}
              prize={(props.prizes)?props.prizes[i]:undefined}
              onClick={() => {
                console.log("test");
                setShowModal(true);
                setNftModal(nft);
                if(props.prizes)
                  setPrizeModal(props.prizes[i]);
              }}
            />
          ))}
        </NftContainer>
      </Container>
    );
  } else {
    return <div> Nothing to show ...</div>;
  }
}

export default ListNFT;
