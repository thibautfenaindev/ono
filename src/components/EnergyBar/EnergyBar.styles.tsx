import styled from "styled-components";

export const Progress = styled.div`
  margin: 20px auto;
  padding: 0;
  width: 90%;
  height: 30px;
  overflow: hidden;
  background: #e5e5e5;
  border-radius: 6px;
`;

export const Bar = styled.div.attrs((props: { width: number }) => props)`
  position: relative;
  float: left;
  min-width: 1%;
  width: ${(props) => props.width}%;
  height: 100%;
  background: #bf161f;
`;
