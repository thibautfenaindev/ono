import { Progress, Bar } from "./EnergyBar.styles";

type EnergyBarProps = {
  minValue?: number,
  maxValue: number,
  value: number
};

function EnergyBar(props: EnergyBarProps) {
  const {minValue = 0} = props;

  function CalculateWidth()
  {
    if(props.value <= minValue)
      return 0;
    if(props.value >= props.maxValue)
      return 100;
    return 100 * (props.value - minValue) / (props.maxValue - minValue);
  }

  return (
    <>
      <Progress>
        <Bar width={CalculateWidth()} />
      </Progress>
    </>
  );
}
export default EnergyBar;
