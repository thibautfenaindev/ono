//import Image from "next/image";
import React, { useState, Suspense } from "react";
import ReactPlayer from "react-player";

import { Container, GameButton, BarContainer } from "./Game.styles";
import { play_game, walletAccount } from "src/utils/Web3Portal";
import TestAnim2 from "@components/Models3D/TestAnim2";
import { Canvas } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import EnergyBar from "@components/EnergyBar/EnergyBar";
import OnoBall from "@components/Models3D/OnoBall";
//import game from "../../../public/assets/game/game.gif";

function Game() {
  const [playing, setPlaying] = useState(false);
  const [playingAnimation, setPlayingAnimation] = useState(false);
  const [width, setWidth] = useState(100)
  const [counter, setCounter] = useState(4)

  return (
    <Container>
      {/* <Image title="GAME" src={game} width={400} /> */}


      <BarContainer>
      <EnergyBar width={width} />

      </BarContainer>

      <Canvas
         camera={{ position: [2, 0, 12.25], fov: 15 }}
         style={{
            backgroundColor: '#ffffff',
            width: '70vw',
            height: '70vh',
            borderRadius: '6px',
         }}
      >
         <ambientLight intensity={1.25} />
         <ambientLight intensity={0.1} />
         <directionalLight intensity={0.4} />
         <Suspense fallback={null}>
            {/* <TestAnim2 isPlaying={playingAnimation} position={[0.025, -0.9, 0] } onAnimationEnd={() => {
              setPlaying(false);
              setPlayingAnimation(false);
            }} /> */}
            <OnoBall />
         </Suspense>
         <OrbitControls enableZoom={true} />
      </Canvas>
      <GameButton
        onClick={async () => {
          
          if( counter !=0) {
            setWidth(width-25);
            setCounter(counter-1)
            setPlaying(true);
          
            await play_game(walletAccount);
            setPlayingAnimation(true);
          }
          console.log('counter', counter)
          if(counter == 0) {
            setPlaying(false)
            alert('You can not play anymore, you do not have any  Energy left, go to our store to buy more or rest till tomorrow')
          }
        }} disabled={playing}
      >
        Play Game
      </GameButton>


    </Container>
  );
}

export default Game;
