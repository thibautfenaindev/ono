import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  max-width: 90vw;
`;

export const Title = styled.h1`
  font-style: bold;
  font-weight: 700;
  font-size: 30px;
  line-height: 34px;
  margin-block-start: 10px;
  margin-left: 100px;
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
`;

export const Content = styled.p`
  margin-block-start: 10px;
  margin-left: 100px;
  margin-top: 20px;

  flex-wrap: wrap;
  align-items: flex-start;
`;

export const OnoUnit = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 10px;
  vertical-align: bottom;
  margin-bottom: 5px;
  margin-left: 5px;
  color: #ff6f5c;
`;
export const OnoValue = styled.span`
  color: #ff6f5c;
`;
