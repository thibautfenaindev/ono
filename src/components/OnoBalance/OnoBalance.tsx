import { ProgressModal } from "@components/Grommets/ProgressModal/ProgressModal";
import {
  ClawMachine__factory,
  OnoToken__factory,
} from "@typechain/ethers-contracts";
import { BigNumber, ethers } from "ethers";
import { Box, Button, Text, TextInput } from "grommet";
import { AppContext } from "pages/_app";
import { useContext, useEffect, useState } from "react";
import { useSigner } from "wagmi";
import {
  Container,
  Content,
  OnoUnit,
  OnoValue,
  Title,
} from "./OnoBalance.styles";

export default function OnoBalance() {
  const { data: signer } = useSigner();
  const [ono, setOno] = useState<BigNumber>();
  const [balance, setBalance] = useState<number>(0);
  const [isApproved, setIsApproved] = useState(0);
  const [isTransfering, setIsTransfering] = useState(false);
  const appContext = useContext(AppContext);

  useEffect(() => {
    if (signer) getBalance();
  }, [signer]);

  useEffect(() => {
    if (isApproved === 3) {
      setIsApproved(0);
      fill();
    }
  }, [isApproved]);

  async function getBalance() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    const clawMachine = ClawMachine__factory.connect(
      process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
      signer
    );
    try {
      let b = await clawMachine.onoDeposit(signer.getAddress());
      setBalance(parseFloat(ethers.utils.formatEther(b)));
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    }
  }

  async function withdraw() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    const clawMachine = ClawMachine__factory.connect(
      process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
      signer
    );
    try {
      setIsTransfering(true);
      let transaction = await clawMachine.withdrawOno();
      let receipt = await transaction.wait();
      console.log(receipt);
      setBalance(0);
    } catch (error: any) {
      console.error(error.message);
      appContext?.setError(error.message);
    } finally {
      setIsTransfering(false);
    }
  }

  async function fill() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    if (!ono || ono.eq(0)) {
      console.error("Ono must be filled");
      return;
    }
    console.log("Connect clawMachine");
    const clawMachine = ClawMachine__factory.connect(
      process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
      signer
    );
    try {
      console.log("Send Ono");
      let transaction = await clawMachine.sendOno(ono);
      let receipt = await transaction.wait();
      console.log(receipt);
      setBalance((v) => parseFloat(ethers.utils.formatEther(ono)) + v);
    } catch (error: any) {
      console.error(error.message);
      appContext?.setError(error.message);
    } finally {
      setIsTransfering(false);
    }
  }

  async function approveOno() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    if (!ono || ono.eq(0)) {
      console.error("Ono must be filled");
      return;
    }

    const onoToken = OnoToken__factory.connect(
      process.env.NEXT_PUBLIC_ONO_TOKEN_ADDRESS!,
      signer
    );
    onoToken.on("Approval", () => {
      setIsApproved((v) => v | 1);
    });

    try {
      setIsTransfering(true);
      let transaction = await onoToken.approve(
        process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
        ono
      );
      let receipt = await transaction.wait();
      console.log(receipt);
      setIsApproved((v) => v | 2);
    } catch (error: any) {
      console.error(error.message);
      appContext?.setError(error.message);
      setIsTransfering(false);
    }
  }
  if (isTransfering) return <ProgressModal text="Ono balance is updating..." />;
  else
    return (
      <Container>
        <Title>Ono</Title>
        <Content>
          Balance : <OnoValue>{balance}</OnoValue>
          <OnoUnit>ONO</OnoUnit>
          <Button
            label={<Text color="white">Withdraw</Text>}
            primary
            color="red"
            style={{ marginLeft: "1em" }}
            disabled={balance == 0 || isTransfering}
            onClick={() => withdraw()}
          />
        </Content>
        <Content>
          <Box direction="row" width="14em">
            <TextInput
              plain
              type="number"
              width="1em"
              onChange={(event) => {
                try {
                  setOno(ethers.utils.parseEther(event.target.value));
                } catch (error) {}
              }}
              style={{ border: "1px solid red" }}
              min="0"
            />
            <Button
              label={<Text color="white">Fill</Text>}
              primary
              color="red"
              style={{ marginLeft: "1em" }}
              disabled={!ono || ono?.eq(0) || isTransfering}
              onClick={() => approveOno()}
            />
          </Box>
        </Content>
      </Container>
    );
}
