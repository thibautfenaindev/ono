import React from 'react';

import { Box, Layer, Spinner, Text } from 'grommet';

type ProgressModalProps = {
  text:string
}

export const ProgressModal = (props: ProgressModalProps) => {
  return (
    <Box pad={{ vertical: 'xlarge' }}>
      <Layer>
        <Box
          align="center"
          justify="center"
          gap="small"
          direction="row"
          alignSelf="center"
          pad="large"
        >
          <Spinner />
          <Text>{props.text}</Text>
        </Box>
      </Layer>
    </Box>
  );
};