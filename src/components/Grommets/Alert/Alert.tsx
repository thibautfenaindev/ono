import React from "react";

import { Box, Button, Heading, Layer, Text } from "grommet";

type AlertProps = {
  children?: React.ReactNode;
  title: string;
  text?: string;
  onClose: () => void;
  onConfirm?: () => void;
};

export const Alert = ({
  title,
  text,
  children,
  onClose,
  onConfirm,
}: AlertProps) => {
  return (
    <Layer
      id="TestInputModal"
      position="center"
      onClickOutside={onClose}
      onEsc={onClose}
    >
      <Box pad="medium" gap="small" width="medium">
        <Heading level={3} margin="none">
          {title}
        </Heading>
        {text && <Text>{text}</Text>}
        {children}
        <Box
          as="footer"
          gap="small"
          direction="row"
          align="center"
          justify="end"
          pad={{ top: "medium", bottom: "small" }}
        >
          {onConfirm && (
            <Button label="Confirm" onClick={onConfirm} color="dark-3" />
          )}
          <Button
            label={
              <Text color="white">
                <strong>Close</strong>
              </Text>
            }
            onClick={onClose}
            primary
            color="status-critical"
          />
        </Box>
      </Box>
    </Layer>
  );
};
