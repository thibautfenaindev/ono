export { default as SEO } from "@components/SEO/SEO";
export { default as ListNft } from "@components/ListNft/ListNft";
export { default as Header } from "@components/Header/Header";
export { default as Game } from "@components/Game/Game";
export { default as NftModal } from "@components/NftModal/NftModal";
export { default as ClawMachineGame } from "@components/Games/ClawMachineGame/ClawMachineGame";
