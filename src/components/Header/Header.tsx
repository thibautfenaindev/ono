import React, { useCallback, useMemo, useState } from "react";

import { navLinksData } from "@config";
import { useHeaderStyle } from "@hooks";
import { HeaderContainer } from "./Header.styles";

import { ConnectButton } from "@rainbow-me/rainbowkit";

import { HeaderProps } from "./Header.types";

import SideMenu from "./SideMenu/SideMenu";
import Navbar from "./Ｎavbar/Navbar";

const Header = ({ activeLink }: HeaderProps) => {
  const { isTop, showSide, setShowSide } = useHeaderStyle();
  const [navLinks] = useState(navLinksData);

  const memoizeToggleMenuCallback = useCallback(() => {
    setShowSide((prevState) => !prevState);
  }, [setShowSide]);

  const memoizeNavLinksData = useMemo(() => {
    return navLinks;
  }, [navLinks]);

  function connectWalletArea() {
    return (
      <ConnectButton
        showBalance={false}
        accountStatus="avatar"
        label="Connect"
      />
    );
  }

  return (
    <HeaderContainer isTop={isTop}>
      <Navbar
        toggleSideMenu={memoizeToggleMenuCallback}
        sideMenuOpen={showSide}
        navLinksData={memoizeNavLinksData}
        activeLink={activeLink}
      />
      <SideMenu
        sideMenuOpen={showSide}
        setShowSide={setShowSide}
        navLinksData={memoizeNavLinksData}
      />

      <div>{connectWalletArea()}</div>
    </HeaderContainer>
  );
};

export default Header;
