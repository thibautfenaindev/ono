import Link from "next/link";
import Burger from "./Burger/Burger";
import NavLogo from "./Logo/NavLogo";
import {
  NavContainer,
  NavLinks,
  NavList,
  NavListIcon,
  NavListItem,
} from "./Navbar.styles";
import React from "react";
import { NavbarProps } from "./Navbar.types";
import Image from "next/image";

function Navbar({
  toggleSideMenu,
  sideMenuOpen,
  navLinksData,
  activeLink,
}: NavbarProps) {
  return (
    <NavContainer>
      <NavLogo />
      <NavLinks>
        <NavList>
          {navLinksData &&
            navLinksData.map(({ id, url, name, element }) => {
              if (name == "twitter") {
                return (
                  <NavListIcon key={id}>
                    <Link
                      href={{
                        pathname: url,
                      }}
                    >
                      <a
                        className={`${activeLink === element ? "active" : ""}`}
                        aria-label={name}
                      >
                        <Image
                          src={"/assets/svg/twitter.svg"}
                          width={20}
                          height={20}
                        />
                      </a>
                    </Link>
                  </NavListIcon>
                );
              }
              if (name == "discord") {
                return (
                  <NavListIcon key={id}>
                    <Link
                      href={{
                        pathname: url,
                      }}
                    >
                      <a
                        className={`${activeLink === element ? "active" : ""}`}
                        aria-label={name}
                      >
                        <Image
                          src={"/assets/svg/discord.svg"}
                          width={20}
                          height={20}
                        />
                      </a>
                    </Link>
                  </NavListIcon>
                );
              }
              return (
                <NavListItem key={id}>
                  <Link href={url} passHref={true}>
                    <a
                      className={`${activeLink === element ? "active" : ""}`}
                      aria-label={name}
                    >
                      {name}
                    </a>
                  </Link>
                </NavListItem>
              );
            })}
        </NavList>
      </NavLinks>
      <Burger toggle={toggleSideMenu} sideMenuOpen={sideMenuOpen} />
    </NavContainer>
  );
}

export default React.memo(Navbar);
