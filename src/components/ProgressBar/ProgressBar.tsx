import { Background, Container, Progress, Static } from "./ProgressBar.styles";

function ProgressBar() {
  return (
    <Container>
      <Background />
      <Progress />
      <Static />
    </Container>
  );
}

export default ProgressBar;
