import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
export const Container = styled.div`
  width: 90%;
  position: relative;
`;

export const BaseBox = styled.div`
  height: 18px;
  position: absolute;
  left: 0;
  top: 0;
  border-radius: 10px;
  border: 1px;
  border-color: #ff6f5c;
  transition: width 10s ease-in-out;
`;

export const StaticBox = styled.div`
  height: 18px;
  position: absolute;
  left: 0;
  top: 0;
  border: 1px;
  transition: width 10s ease-in-out;
`;

export const Background = styled(BaseBox)`
  width: 100%;
  height: 18px;
  border: solid 2px;
  border-color: #ff6f5c;
`;

export const Static = styled(StaticBox)`
  border-right: solid 2px #ff6f5c;
  width: 80%;
`;

export const Progress = styled(BaseBox)`
  background: #ff6f5c;
  width: 60%;
`;
