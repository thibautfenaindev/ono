import styled from "styled-components";

export const ProfileContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  height: 100vh;
  width: 90vw;
  align-items: center;
`;
export const Container = styled.div`
  display: flex;
  flex-direction: row;
  text-align: center;
  justify-content: center;
  height: 100vh;
  align-items: center;
`;

export const Description = styled.div`
  font-family: "Syncopate";
  font-style: normal;
  align-items: center;
  font-weight: 700;
  font-size: 15px;
  max-width: 60vw;
  height: 300px;
  margin-left: 20px;
`;

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  margin-right: 200px;
`;

export const ImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  padding-bottom: 10px;
`;

export const MenuButton = styled.button`
  color: #ffffff;
  width: 15vw;
  height: 8vh;
  margin-bottom: 70px;
  border-radius: 5px;
  border-color: white;
  font-family: "Helvetica Neue";
  background: linear-gradient(
    90deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(14, 17, 22, 1) 15%
  );
`;

export const MenuButtonL = styled.button`
  color: #ffffff;
  width: 15vw;
  height: 8vh;
  border-radius: 5px;
  border-color: white;
  font-family: "Helvetica Neue";
  font-style: bold;
  font-size: 15px;
  background: linear-gradient(
    90deg,
    rgba(255, 255, 255, 0) 0%,
    rgba(14, 17, 22, 1) 15%
  );
`;
