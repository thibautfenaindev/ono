import {
  ButtonContainer,
  Container,
  ImageContainer,
  MenuButton,
  MenuButtonL,
  ProfileContainer,
} from "./Profile.styles";
import Image from "next/image";
import game from "../../../public/assets/claw.png";

function Profile() {
  return (
    <ProfileContainer>
      <Container>
        <ButtonContainer>
          <MenuButton>PROFILE</MenuButton>
          <MenuButton>LIST</MenuButton>
          <MenuButtonL>GAME</MenuButtonL>
        </ButtonContainer>
        <ImageContainer>
          <Image src={game} height={400} width={400} />
        </ImageContainer>
      </Container>
    </ProfileContainer>
  );
}

export default Profile;
