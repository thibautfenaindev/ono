import {
  ClawMachine,
  ClawMachine__factory,
  IERC721,
  IERC721__factory,
} from "@typechain/ethers-contracts";
import { Nft } from "alchemy-sdk";
import { ethers } from "ethers";
import { useContext, useEffect, useState } from "react";
import { useAccount, useSigner } from "wagmi";
import {
  ImageNft,
  ListContainer,
  MainContainer,
  MetadaContainer,
  NftContainer,
  StyledModal,
  TitleNft,
  OwnerTitle,
  RewardTitle,
  RewardScore,
  Footer,
  ButtonContainer,
  ButtonUnlist,
  RewardUnit,
  ScoreContainer,
  CloseCross,
} from "./NftModal.styles";

import { ProgressModal } from "@components/Grommets/ProgressModal/ProgressModal";

import { useRouter } from "next/router";
import { Alert } from "@components/Grommets/Alert/Alert";
import { TextInput } from "grommet";
import { AppContext } from "pages/_app";

type NftModalProps = {
  nft: Nft | undefined;
  prize: ClawMachine.PrizeStructOutput | undefined;
  isOpen: boolean;
  toggleModal: () => void;
};

function NftModal(props: NftModalProps) {
  console.log("test modal", props);
  const imgSrc = props.nft?.rawMetadata?.image;
  const name = props.nft!["title"];

  const [isTransfering, setTransfering] = useState(false);
  const [expectedIncomeOpened, setExpectedIncomeOpened] = useState(false);
  const [expectedIncome, setExpectedIncome] = useState<string>();
  const router = useRouter();
  const appContext = useContext(AppContext);

  const { address } = useAccount();
  const { data: signer } = useSigner();

  let IERC721: IERC721;
  let clawMachine: ClawMachine;

  useEffect(() => {
    setExpectedIncome(
      ethers.utils.formatEther(props.prize?.expectedIncome ?? 0)
    );
  }, []);

  async function deposit() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    setTransfering(true);
    IERC721 = IERC721__factory.connect(props.nft?.contract.address!, signer);
    const fromAddress = await signer.getAddress();
    console.log("from address : " + fromAddress);
    //const filter = IERC721.filters.Transfer(fromAddress, process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS, props.nft?.tokenId);
    IERC721.on("Transfer", (from, to, tokenId) => {
      console.log(from, to, tokenId);
    });
    try {
      let transaction = await IERC721[
        "safeTransferFrom(address,address,uint256)"
      ](
        signer.getAddress(),
        process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
        props.nft?.tokenId!
      );
      let receipt = await transaction.wait();
      console.log(receipt);
      router.push("/AllPrizes");
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    } finally {
      setTransfering(false);
    }
  }

  async function retrieve() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    setTransfering(true);
    try {
      clawMachine = ClawMachine__factory.connect(
        process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
        signer
      );

      clawMachine.on("PrizeRetrieved", () => {
        console.log("Prize retrieved");
      });

      let id = ethers.utils.keccak256(
        ethers.utils.defaultAbiCoder.encode(
          ["address", "uint"],
          [props.prize?.nftAddress, props.prize?.tokenId]
        )
      );
      let transaction = await clawMachine.retrievePrize(id);
      let receipt = await transaction.wait();
      console.log(receipt);
      setTransfering(false);
      router.push("/List");
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    } finally {
      setTransfering(false);
    }
  }

  async function stakePrize(expectedIncome: string | undefined) {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    setTransfering(true);
    try {
      clawMachine = ClawMachine__factory.connect(
        process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
        signer
      );
      let id = ethers.utils.keccak256(
        ethers.utils.defaultAbiCoder.encode(
          ["address", "uint"],
          [props.prize?.nftAddress, props.prize?.tokenId]
        )
      );
      let transaction = await clawMachine.stakePrize(
        id,
        ethers.utils.parseEther(expectedIncome ?? "0")
      );
      let receipt = await transaction.wait();
      console.log(receipt);
      router.reload();
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    } finally {
      setTransfering(false);
    }
  }

  async function unstakePrize() {
    if (signer == null) {
      console.error("Signer not found !");
      return;
    }
    setTransfering(true);
    try {
      clawMachine = ClawMachine__factory.connect(
        process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!,
        signer
      );
      let id = ethers.utils.keccak256(
        ethers.utils.defaultAbiCoder.encode(
          ["address", "uint"],
          [props.prize?.nftAddress, props.prize?.tokenId]
        )
      );
      let transaction = await clawMachine.unstakePrize(id);
      let receipt = await transaction.wait();
      console.log(receipt);
      router.reload();
    } catch (error: any) {
      console.error(error);
      appContext?.setError(error.message);
    } finally {
      setTransfering(false);
    }
  }

  const isIpfs = (imgSrc: string | undefined) => {
    if (imgSrc == null) return "";

    if (imgSrc?.startsWith("ipfs://"))
      return "https://ipfs.io/ipfs" + imgSrc.slice(6);
    return imgSrc;
  };

  if (isTransfering) return <ProgressModal text="The NFT is transfering..." />;
  else if (expectedIncomeOpened)
    return (
      <Alert
        title="Confirm"
        text="What is your expected income ?"
        onClose={() => setExpectedIncomeOpened(false)}
        onConfirm={() => stakePrize(expectedIncome)}
      >
        <TextInput
          plain
          type="number"
          value={expectedIncome}
          onChange={(event) => setExpectedIncome(event.target.value)}
        />
      </Alert>
    );
  else
    return (
      <StyledModal
        isOpen={props.isOpen}
        onBackgroundClick={props.toggleModal}
        onEscapeKeydown={props.toggleModal}
      >
        <CloseCross onClick={props.toggleModal}> X </CloseCross>
        <MainContainer>
          <NftContainer>
            <ImageNft src={isIpfs(imgSrc)} width={200} height={200} />
            <TitleNft>{name}</TitleNft>
          </NftContainer>

          <MetadaContainer>
            {props.prize && (
              <>
                <ListContainer>
                  <OwnerTitle>Owner</OwnerTitle>
                  <ScoreContainer>
                    <RewardScore>
                      {" "}
                      {props.prize.ownerAddress == address
                        ? "Me"
                        : props.prize.ownerAddress}
                    </RewardScore>
                  </ScoreContainer>
                </ListContainer>
                <ListContainer>
                  <RewardTitle>EXPECTED INCOME</RewardTitle>
                  <ScoreContainer>
                    <RewardScore> {expectedIncome} </RewardScore>
                    <RewardUnit> ONO</RewardUnit>
                  </ScoreContainer>
                </ListContainer>
              </>
            )}
          </MetadaContainer>
        </MainContainer>
        <Footer>
          <ButtonContainer>
            {props.prize ? (
              props.prize.ownerAddress == address && (
                <>
                  <ButtonUnlist
                    onClick={() => {
                      retrieve();
                    }}
                  >
                    {" Retrieve "}
                  </ButtonUnlist>

                  <ButtonUnlist
                    onClick={() => {
                      if (props.prize?.status == 0)
                        setExpectedIncomeOpened(true);
                      else unstakePrize();
                    }}
                  >
                    {props.prize?.status == 0 ? " Stake " : " Unstake "}
                  </ButtonUnlist>
                </>
              )
            ) : (
              <ButtonUnlist
                onClick={() => {
                  deposit();
                }}
              >
                {" Deposit "}
              </ButtonUnlist>
            )}
          </ButtonContainer>
        </Footer>
      </StyledModal>
    );
}

export default NftModal;
