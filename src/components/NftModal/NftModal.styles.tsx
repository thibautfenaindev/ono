import styled from "styled-components";
import Modal from "styled-react-modal";

export const MainContainer = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  margin-top: 30px;
  margin-bottom: 20px;
  margin-left: 50px;
`;

export const NftContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  color: white;
  height: 220px;
  margin-top: 10px;
  margin-right: 10px;
`;

export const CloseCross = styled.div`
  font-family: "Arial";
  color: #ff6f5c;
  font-weight: bold;
  width: 95%;
  padding-top: 20px;
  text-align: end;
`;

export const ImageNft = styled.img`
  border-radius: 15px;
  margin-bottom: 10px;
`;

export const TitleNft = styled.div`
  font-family: "Arial";
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  line-height: 28px;

  color: #ffffff;
`;

export const MetadaContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  height: 200px;
  margin-left: 10px;
  margin-bottom: 30px;
`;

export const StyledModal = Modal.styled`
  display: flex;
  flex-direction: column;
  align-items: start;
  justify-content: center;
  opacity: 0.9;
  background-color: rgba(0,0,0.3);
  border: solid 2px #ff6f5c;
  border-radius: 25px;
  overlay: { background: 'black' }

`;

export const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5px;
  margin-bottom: 5px;
  margin-right: 50px;
  justify-content: start;
  align-items: flex-start;
`;

export const OwnerTitle = styled.div`
  font-size: 8px;
  line-height: 13px;
  color: #ff6f5c;
`;

export const AttempsTitle = styled.div`
  font-family: "Arial";
  font-size: 12px;
  color: #d1d1d1;
`;

export const RewardTitle = styled.div`
  font-style: normal;
  font-size: 10px;
  line-height: 13px;
  color: #ff6f5c;
`;

export const ScoreContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
`;
export const RewardScore = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  color: #ff6f5c;
  vertical-align: bottom;
`;

export const RewardUnit = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 10px;
  vertical-align: bottom;
  margin-bottom: 5px;
  margin-left: 5px;
  color: #ff6f5c;
`;

export const CollectionScore = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  color: white;
  vertical-align: bottom;
`;

export const CollectionUnit = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 10px;
  vertical-align: bottom;
  margin-bottom: 5px;
  margin-left: 5px;
  color: white;
`;

export const CollectionTitle = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 13px;
  line-height: 13px;
  color: #ffffff;
`;

export const FloorTitle = styled.div`
  font-style: normal;
  font-size: 10px;
  color: #ffffff;
`;

export const FloorScore = styled.div`
  font-family: "Syncopate Bold";
  font-size: 20px;
  width: 200px;
  color: #ffffff;
`;

export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: end;
  width: 550px;
  height: 70px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;

  background: linear-gradient(
    274.32deg,
    rgba(241, 242, 242, 0.3) 20.5%,
    rgba(128, 130, 133, 0.1) 41.86%
  );
`;

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 220px;
  margin-bottom: 10px;
`;

export const ButtonUnlist = styled.button`
  border: 1px solid #ff6f5c;
  text-align: center;
  background-color: #000000;
  color: #ff6f5c;
  padding: 12px;
  font-size: 20px;
  font-weight: 700;
  float: right;
  &:hover {
    border: 1px solid #ffffff;
  }
`;

export const ExpectedPriceContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 50px;
  margin-bottom: 30px;
  width: 300px;
`;
export const ExpectedPriceTitle = styled.div`
  font-family: "Syncopate Bold";
  font-style: normal;
  font-size: 12px;
  margin-bottom: 10px;
  line-height: 20px;
  color: #ff6f5c;
`;
