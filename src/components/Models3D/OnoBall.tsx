import { useLoader } from '@react-three/fiber'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'

function OnoBall() {
    const fbx = useLoader(FBXLoader, '/onoBall.fbx')
    return <primitive object={fbx} />
}

export default OnoBall