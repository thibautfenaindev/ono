import { DefaultTheme } from "styled-components";

const defaultTheme: DefaultTheme = {
  // Temp fonts
  fonts: {
    title: "'Syncopate', sans-serif",
    main: "'Syncopate Bold', sans-serif",
    mono: "'Syncopate', sans-serif",
  },
  // Colors for layout
  colors: {
    background_main: "black",
    text: {
      primary: "white",
    },
    header: {
      background: {
        top: "black",
        fixed: "black",
      },
      link: {
        primary: "white",
        hover: "white",
        active: "#D10028",
      },
    },
  },
};

export { defaultTheme };
