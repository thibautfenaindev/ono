import { SEO, ClawMachineGame } from "@components";
import Layout from "@layout/Layout";
import type { GetStaticProps, NextPage } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Header from "@components/Header/Header";

import { useAccount } from "wagmi";
import { useState, useEffect } from "react";
import useNfts from "@hooks/useNfts";

const Games: NextPage = () => {
  const [activeLink] = useState<string>("games");
  const [_isConnected, setConnected] = useState(false);

  const { isConnected } = useAccount();
  const { nfts, prizes } = useNfts(
    process.env.NEXT_PUBLIC_CLAW_MACHINE_ADDRESS!
  );

  useEffect(() => {
    setConnected(isConnected);
  }, [isConnected]);

  const { t } = useTranslation("footer");
  return (
    <>
      <Layout>
        <Header activeLink={activeLink} />
        <SEO />
        {_isConnected ? (
          <ClawMachineGame nfts={nfts} prizes={prizes} />
        ) : (
          <div>
            <p>{t("common:welcome")}</p>
            <p> Please log in to access the app</p>
          </div>
        )}
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslations(
        context?.locale === undefined ? "" : context.locale,
        ["common", "about"]
      )),
      // Will be passed to the page component as props
    },
  };
};

export default Games;
