import Theme from "styles/Theme";
import { appWithTranslation } from "next-i18next";
import { alchemyProvider } from "wagmi/providers/alchemy";
import { ModalProvider } from "styled-react-modal";
import { createClient, configureChains, chain, WagmiConfig } from "wagmi";
import { SessionProvider } from "next-auth/react";
import { Grommet, Notification } from "grommet";

import { publicProvider } from "wagmi/providers/public";
import {
  getDefaultWallets,
  RainbowKitProvider,
  darkTheme,
} from "@rainbow-me/rainbowkit";
import "@rainbow-me/rainbowkit/styles.css";
import { config } from "@config/AlchemyConfig";

import type { AppProps } from "next/app";
import { createContext, useState } from "react";

type AppContextType = {
  setError: (e: string) => void;
};
export const AppContext = createContext<AppContextType | null>(null);

function App({ Component, pageProps }: AppProps) {
  const [error, setError] = useState<string>("");
  const appContextValue: AppContextType = {
    setError: setError,
  };

  const { chains, provider, webSocketProvider } = configureChains(
    [chain.polygonMumbai],
    [alchemyProvider({ apiKey: config.apiKey }), publicProvider()]
  );

  const { connectors } = getDefaultWallets({
    appName: "My RainbowKit App",
    chains,
  });

  const client = createClient({
    autoConnect: true,
    connectors,
    provider,
    webSocketProvider,
  });

  return (
    <WagmiConfig client={client}>
      <SessionProvider refetchInterval={0} session={pageProps.session}>
        <RainbowKitProvider
          chains={chains}
          theme={darkTheme({
            accentColor: "#BF161F",
            accentColorForeground: "white",
            borderRadius: "large",
            fontStack: "system",
            overlayBlur: "large",
          })}
        >
          <Grommet>
            <Theme>
              <ModalProvider>
                <AppContext.Provider value={appContextValue}>
                  <Component {...pageProps} />
                  {error != "" && (
                    <Notification
                      toast
                      status="critical"
                      title="Error"
                      message={error}
                      onClose={() => setError("")}
                    />
                  )}
                </AppContext.Provider>
              </ModalProvider>
            </Theme>
          </Grommet>
        </RainbowKitProvider>
      </SessionProvider>
    </WagmiConfig>
  );
}

export default appWithTranslation(App);
