import { SEO } from "@components";
import Layout from "@layout/Layout";
import type { GetStaticProps, NextPage } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Header from "@components/Header/Header";

import ListNFT from "@components/ListNft/ListNft";
import { useState, useEffect} from "react";
import { useAccount } from "wagmi";
import useNfts from "@hooks/useNfts";

const List: NextPage = () => {
  const [activeLink] = useState<string>("list_nft");
  const [_isConnected, setConnected] = useState(false);
  
  const { isConnected, address } = useAccount();
  const {nfts} = useNfts(address!);

  useEffect(() => {
    setConnected(isConnected);
  }, [isConnected]);

  const { t } = useTranslation("footer");
  return (
    <>
      <Header activeLink={activeLink} />

      <Layout>
        <SEO />
        {_isConnected ? (
          <div>
            <ListNFT
              id="list_nft"
              title="My NFT's"
              nfts={nfts}
              visibility={activeLink == "list_nft"}
              prizes={undefined}
            />
          </div>
        ) : (
          <div>
            <p>{t("common:welcome")}</p>
            <p> Please log in to access the app</p>
          </div>
        )}
      </Layout>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      ...(await serverSideTranslations(
        context?.locale === undefined ? "" : context.locale,
        ["common", "about"]
      )),
      // Will be passed to the page component as props
    },
  };
};

export default List;
